package com.poznyak.springweb.web.controller;

import com.poznyak.springweb.dto.AddressDto;
import com.poznyak.springweb.entity.Address;
import com.poznyak.springweb.exception.NotFoundException;
import com.poznyak.springweb.mapper.AddressMapper;
import com.poznyak.springweb.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/addresses")
public class AddressController {

    private final AddressService addressService;
    private final AddressMapper mapper;

    @Autowired
    public AddressController(AddressService addressService, AddressMapper mapper) {
        this.addressService = addressService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressDto> getAddress(@PathVariable("id") Long id) throws NotFoundException {
        Address address = addressService.getAddress(id);
        AddressDto addressDto = mapper.toDto(address);
        return new ResponseEntity<>(addressDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AddressDto> saveAddress(@RequestBody AddressDto addressDto) {
        Address address = mapper.toEntity(addressDto);
        Address savedAddress = addressService.saveAddress(address);
        AddressDto savedAddressDto = mapper.toDto(savedAddress);
        return new ResponseEntity<>(savedAddressDto, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AddressDto> updateAddress(@PathVariable("id") Long id, @RequestBody AddressDto addressDto) {
        Address address = mapper.toEntity(addressDto);
        Address updatedAddress = addressService.updateAddress(id, address);
        AddressDto updatedAddressDto = mapper.toDto(updatedAddress);
        return new ResponseEntity<>(updatedAddressDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<AddressDto> deleteAddress(@PathVariable("id") Long id) throws NotFoundException {
        this.addressService.deleteAddress(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}