package com.poznyak.springweb.web.controller;

import com.poznyak.springweb.dto.UserDto;
import com.poznyak.springweb.entity.User;
import com.poznyak.springweb.exception.NotFoundException;
import com.poznyak.springweb.mapper.UserMapper;
import com.poznyak.springweb.service.DefaultUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final DefaultUserService userService;
    private final UserMapper mapper;

    @Autowired
    public UserController(DefaultUserService userService, UserMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findUser(@PathVariable("id") Long id) {
        User user = userService.getUser(id);
        UserDto userDto = mapper.toDto(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto) {
        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable("id") Long id, @RequestBody UserDto userDto) {
        User user = mapper.toEntity(userDto);
        User updatedUser = userService.updateUser(id, user);
        UserDto updatedUserDto = mapper.toDto(user);
        return new ResponseEntity<>(updatedUserDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable("id") Long id) throws NotFoundException {
        this.userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
