package com.poznyak.springweb.web.controller;

import com.poznyak.springweb.dto.ItemDto;
import com.poznyak.springweb.entity.Item;
import com.poznyak.springweb.exception.NotFoundException;
import com.poznyak.springweb.mapper.ItemMapper;
import com.poznyak.springweb.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/items")
public class ItemController {

    private final ItemService itemService;
    private final ItemMapper mapper;

    @Autowired
    public ItemController(ItemService itemService, ItemMapper mapper) {
        this.itemService = itemService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ItemDto> findItem(@PathVariable("id") Long id) {
        Item item = itemService.getItem(id);
        ItemDto itemDto = mapper.toDto(item);
        return new ResponseEntity<>(itemDto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<ItemDto> saveItem(@RequestBody ItemDto itemDto) {
        Item item = mapper.toEntity(itemDto);
        Item savedItem = itemService.saveItem(item);
        ItemDto savedItemDto = mapper.toDto(savedItem);
        return new ResponseEntity<>(savedItemDto, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ItemDto> updateItem(@PathVariable("id") Long id, @RequestBody ItemDto itemDto) {
        Item item = mapper.toEntity(itemDto);
        Item updatedItem = itemService.updateItem(id, item);
        ItemDto updatedItemDto = mapper.toDto(updatedItem);
        return new ResponseEntity<>(updatedItemDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ItemDto> deleteUser(@PathVariable("id") Long id) throws NotFoundException {
        this.itemService.deleteItem(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}


