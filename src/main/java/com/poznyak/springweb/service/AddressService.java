package com.poznyak.springweb.service;

import com.poznyak.springweb.entity.Address;
import com.poznyak.springweb.exception.NotFoundException;

import java.util.List;

public interface AddressService {

    Address getAddress(Long id) throws NotFoundException;

    Address saveAddress(Address address);

    Address updateAddress(Long id, Address address) throws NotFoundException;

    void deleteAddress(Long id);

    List<Address> getAddresses();

    void deleteAddresses();
}