package com.poznyak.springweb.service;

import com.poznyak.springweb.entity.Item;
import com.poznyak.springweb.exception.NotFoundException;

import java.util.List;

public interface ItemService {

    Item getItem(Long id) throws NotFoundException;

    Item saveItem(Item item);

    List<Item> getItems();

    Item updateItem(Long id, Item item) throws NotFoundException;

    void deleteItem(Long id);

    void deleteItems();
}