package com.poznyak.springweb.service;

import com.poznyak.springweb.entity.Item;
import com.poznyak.springweb.exception.NotFoundException;
import com.poznyak.springweb.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultItemService implements ItemService {

    private final ItemRepository itemRepository;


    @Autowired
    public DefaultItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    @Transactional
    public Item getItem(Long id) throws NotFoundException {
        Optional<Item> item = itemRepository.findById(id);
        if (item.isPresent()) {
            return item.get();
        } else {
            throw new NotFoundException("Item with id: " + id + " was not found");
        }
    }

    @Override
    @Transactional
    public Item saveItem(Item item) {
        return itemRepository.save(item);
    }

    @Override
    @Transactional
    public List<Item> getItems() {
        return itemRepository.findAll();
    }

    @Override
    @Transactional
    public Item updateItem(Long id, Item item) throws NotFoundException {
        Optional<Item> itemToUpdateOptional = itemRepository.findById(id);
        if (itemToUpdateOptional.isPresent()) {
            Item itemToUpdate = this.updateItemFields(itemToUpdateOptional.get(), item);
            return itemRepository.save(itemToUpdate);
        } else {
            throw new NotFoundException("Item with id: " + id + " was not found");
        }
    }

    @Override
    @Transactional
    public void deleteItem(Long id) {
        itemRepository.deleteById(id);
    }

    @Override
    public void deleteItems() {
        itemRepository.deleteAll();
    }

    private Item updateItemFields(Item itemToUpdate, Item newItem) {
        if (newItem.getTitle() != null) {
            itemToUpdate.setTitle(newItem.getTitle());
        }

        if (newItem.getUser() != null) {
            itemToUpdate.setUser(newItem.getUser());
        }

        return itemToUpdate;
    }
}