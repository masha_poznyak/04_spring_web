package com.poznyak.springweb.service;

import com.poznyak.springweb.entity.Address;
import com.poznyak.springweb.exception.NotFoundException;
import com.poznyak.springweb.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultAddressService implements AddressService {

    private final AddressRepository addressRepository;

    @Autowired
    public DefaultAddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    @Transactional
    public Address getAddress(Long id) throws NotFoundException {
        Optional<Address> address = addressRepository.findById(id);
        if (address.isPresent()) {
            return address.get();
        } else {
            throw new NotFoundException("Address with id: " + id + " was not found");
        }
    }

    @Override
    @Transactional
    public Address saveAddress(Address address) {
        return addressRepository.save(address);
    }

    @Override
    @Transactional
    public List<Address> getAddresses() {
        return addressRepository.findAll();
    }

    @Override
    @Transactional
    public Address updateAddress(Long id, Address address) throws NotFoundException {
        Optional<Address> addressToUpdateOptional = addressRepository.findById(id);
        if (addressToUpdateOptional.isPresent()) {
            Address addressToUpdate = this.updateAddressFields(addressToUpdateOptional.get(), address);
            return addressRepository.save(addressToUpdate);
        } else {
            throw new NotFoundException("Address with id: " + id + " was not found");
        }
    }

    @Override
    @Transactional
    public void deleteAddress(Long id) {
        if (getAddress(id).getUser() == null) {
            addressRepository.deleteById(id);
        } else {
            getAddress(id).getUser().setAddress(null);
        }
    }

    @Override
    public void deleteAddresses() {
        addressRepository.deleteAll();
    }

    private Address updateAddressFields(Address addressToUpdate, Address newAddress) {
        if (newAddress.getCity() != null) {
            addressToUpdate.setCity(newAddress.getCity());
        }

        if (newAddress.getStreet() != null) {
            addressToUpdate.setStreet(newAddress.getStreet());
        }

        return addressToUpdate;
    }
}