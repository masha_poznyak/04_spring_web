package com.poznyak.springweb.service;

import com.poznyak.springweb.entity.User;
import com.poznyak.springweb.exception.NotFoundException;
import com.poznyak.springweb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public DefaultUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUser(Long id) throws NotFoundException {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new NotFoundException("User with id: " + id + " was not found");
        }
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User updateUser(Long id, User user) throws NotFoundException {
        Optional<User> userToUpdateOptional = userRepository.findById(id);
        if (userToUpdateOptional.isPresent()) {
            User userToUpdate = this.updateUserFields(userToUpdateOptional.get(), user);
            return userRepository.save(userToUpdate);
        } else {
            throw new NotFoundException("User with id: " + id + " was not found");
        }
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteUsers() {
        userRepository.deleteAll();
    }

    private User updateUserFields(User userToUpdate, User newUser) {
        if (newUser.getName() != null) {
            userToUpdate.setName(newUser.getName());
        }
        if (newUser.getAddress().getCity() != null) {
            userToUpdate.getAddress().setCity(newUser.getAddress().getCity());
        }
        if (newUser.getAddress().getStreet() != null) {
            userToUpdate.getAddress().setStreet(newUser.getAddress().getStreet());
        }
        return userToUpdate;
    }
}
