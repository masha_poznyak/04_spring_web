package com.poznyak.springweb.service;

import com.poznyak.springweb.entity.User;
import com.poznyak.springweb.exception.NotFoundException;

import java.util.List;

public interface UserService {

    User getUser(Long id) throws NotFoundException;

    User saveUser(User user);

    List<User> getUsers();

    User updateUser(Long id, User user);

    void deleteUser(Long id);

    void deleteUsers();
}
