package com.poznyak.springweb.repository;

import com.poznyak.springweb.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
