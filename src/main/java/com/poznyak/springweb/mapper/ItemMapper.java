package com.poznyak.springweb.mapper;

import com.poznyak.springweb.dto.ItemDto;
import com.poznyak.springweb.entity.Item;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ItemMapper {

    private final ModelMapper mapper;

    @Autowired
    public ItemMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Item toEntity(ItemDto ItemDto) {
        return Objects.isNull(ItemDto) ? null : mapper.map(ItemDto, Item.class);
    }

    public ItemDto toDto(Item entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, ItemDto.class);
    }
}


