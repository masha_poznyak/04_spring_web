package com.poznyak.springweb.dto;

import lombok.Data;

@Data
public class ItemDto {
    private String id;
    private String title;
    private String user;
}