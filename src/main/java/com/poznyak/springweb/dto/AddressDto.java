package com.poznyak.springweb.dto;

import lombok.Data;

@Data
public class AddressDto {
    private String id;
    private String street;
    private String city;
}
